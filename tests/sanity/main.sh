#!/bin/bash

# Copyright (c) 2016 Red Hat, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Author: Yi Zhang  <yizhan@redhat.com>

#set -x
source ../include/tc.sh || exit 200

tlog "running $0"

tok "yum -y reinstall ndctl"
tok "yum -y remove ndctl"
tok "yum -y install ndctl"
tok "which ndctl"
tok "yum info ndctl"
tok "[[ -f "/usr/bin/ndctl" ]]"
tok "ndctl --list-cmds"
tok "ndctl version"
